# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Base class for list pages
#
# Copyright 2009 Ge van Geldorp
# Copyright 2014, 2017-2018, 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package ObjectModel::CGI::CollectionPage;

=head1 NAME

ObjectModel::CGI::CollectionPage - Base class for list pages

A page containing a single collection block.

The CollectionPage instances have the following properties:
=over
=item Collection
The collection containing the items to be shown in the CollectionBlock widget.

=item CollectionBlock
The widget showing the items in the specified collection.

=item ActionPerformed
Set to true if an action was specified and succeeded.
=back

=cut

use ObjectModel::CGI::Page;
our @ISA = qw(ObjectModel::CGI::Page);

# Use require to avoid overriding Page::new()
require ObjectModel::CGI::CollectionBlock;


=pod
=over 12

=item C<_initialize()>

Sets up the new page object.

Parameters:
=over

=item Request
=item RequiredRole
See Page::new().

=item Collection
This is the collection of items that should be shown in the CollectionBlock
widget.

=item BlockCreator
If undefined the page will create an instance of the CollectionBlock class.
Otherwise it will use this function to create the CollectionBlock widget, thus
enabling use of a CollectionBlock subclass.

=back

=back
=cut

sub _initialize($$$$;$)
{
  my ($self, $Request, $RequiredRole, $Collection, $BlockCreator) = @_;

  $self->{Collection} = $Collection;
  $BlockCreator ||= \&ObjectModel::CGI::CollectionBlock::Create;
  $self->{CollectionBlock} = &$BlockCreator($Collection, $self);
  $self->SUPER::_initialize($Request, $RequiredRole);
}

sub SetReadWrite($$)
{
  my ($self, $RW) = @_;

  $self->{CollectionBlock}->SetReadWrite($RW);
}


#
# HTML page generation
#

sub GetTitle($)
{
  my ($self) = @_;

  my $Title = ucfirst($self->{Collection}->GetCollectionName());
  $Title =~ s/([a-z])([A-Z])/$1 $2/g;
  return $Title;
}

sub GenerateTitle($)
{
  my ($self) = @_;

  my $Title = $self->GetTitle();
  if ($Title)
  {
    print "<h1 id='PageTitle'>", $self->escapeHTML($Title), "</h1>\n";
  }
}

sub GenerateBody($)
{
  my ($self) = @_;

  print "<div class='CollectionPageBody'>\n";
  $self->GenerateTitle();
  print "<div class='Content'>\n";
  $self->{CollectionBlock}->GenerateList();
  print "</div>\n";
  print "</div>\n";
}

sub GeneratePage($)
{
  my ($self) = @_;

  if ($self->GetParam("Action"))
  {
    $self->{ActionPerformed} = $self->{CollectionBlock}->OnAction($self->GetParam("Action"));
  }
  $self->SUPER::GeneratePage();
}

1;
