# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Generates HTML providing a user-readable representation of a given value
#
# Copyright 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package ObjectModel::CGI::ValueFormatter;

use Exporter 'import';
our @EXPORT = qw(GetDateTimeJSFile GenerateDateTime GenerateTipDateTime
                 GenerateDayTimeTipDate GenerateTimeTipDate GenerateValueHTML);

use POSIX qw(strftime);


#
# Timestamp formatting
#

=pod
=over 12

=item C<GetDateTimeJSFile()>

Returns the path to the file containing the JavaScript code to format
timestamps.

See GenerateDateTime().

=back
=cut

sub GetDateTimeJSFile()
{
  return "/js/datetime.js";
}

=pod
=over 12

=item C<GenerateDateTime()>

Returns an HTML snippet that uses the JavaScript ShowDateTime() function
to show a timestamp in ISO 8601 format in the user's local timezone.

Note that the timestamp will be shown based on the server's timezone in case
JavaScript is disabled.
=over

=item Sec1970
The timestamp to display as the number of seconds since the Epoch.

=item Class
A string containing additional classes for the <span> tag used to show the
date + time.

=item TagAttrs
A string containing the tag name without the initial bracket, and any
additional attributes except class and timestamp. By default this is 'span'.

=back
=back
=cut

sub GenerateDateTime($;$$)
{
  my ($Sec1970, $Class, $TagAttrs) = @_;

  if (defined $Sec1970)
  {
    my $Tag;
    if ($TagAttrs)
    {
      $Tag = $TagAttrs;
      $Tag =~ s/ .*$//;
    }
    else
    {
      $TagAttrs = $Tag = "span";
    }
    $Class = "$Class " if ($Class);
    print "<$TagAttrs class='${Class}datetime' timestamp='$Sec1970'>",
          strftime("%Y-%m-%d %H:%M:%S", localtime($Sec1970)), "</$Tag>";
  }
  else
  {
    print "&nbsp;";
  }
}

=pod
=over 12

=item C<GenerateTipDateTime()>

Adds a tooltip containing the timestamp to the specified HTML chunk.

The timestamp is converted to the ISO 8601 format in the user's timezone if
JavaScript is available and in the server's timezone otherwise.

The default for TagAttrs is 'a' and it should always be a tag that shows the
content of the title attribute as a tooltip.

See GenerateDateTime() for more details.

=back
=cut

sub GenerateTipDateTime($$;$$)
{
  my ($Sec1970, $Html, $Class, $TagAttrs) = @_;

  if (defined $Sec1970)
  {
    my $Tag;
    if ($TagAttrs)
    {
      $Tag = $TagAttrs;
      $Tag =~ s/ .*$//;
    }
    else
    {
      $TagAttrs = $Tag = "a";
    }
    $Class = "$Class " if ($Class);
    print "<$TagAttrs class='${Class}tipdatetime' timestamp='$Sec1970' ",
          strftime("title='%Y-%m-%d %H:%M:%S'>", localtime($Sec1970)),
          "$Html</$Tag>";
  }
  else
  {
    print $Html;
  }
}

=pod
=over 12

=item C<GenerateDayTimeTipDate()>

Show the timestamp's day and time with the date as a tooltip.

The timestamp is shown in the user's timezone if JavaScript is available and
in the server's timezone otherwise.

The default for TagAttrs is 'a' and it should always be a tag that shows the
content of the title attribute as a tooltip.

See GenerateDateTime() for more details.

=back
=cut

sub GenerateDayTimeTipDate($;$$)
{
  my ($Sec1970, $Class, $TagAttrs) = @_;

  if (defined $Sec1970)
  {
    my $Tag;
    if ($TagAttrs)
    {
      $Tag = $TagAttrs;
      $Tag =~ s/ .*$//;
    }
    else
    {
      $TagAttrs = $Tag = "a";
    }
    $Class = "$Class " if ($Class);
    print "<$TagAttrs class='${Class}daytimetipdate' timestamp='$Sec1970' ",
          strftime("title='%Y-%m-%d'>%d&nbsp;%H:%M:%S", localtime($Sec1970)),
          "</$Tag>";
  }
  else
  {
    print "&nbsp;";
  }
}

=pod
=over 12

=item C<GenerateTimeTipDate()>

Show the timestamp's time with the date as a tooltip.

The timestamp is shown in the user's timezone if JavaScript is available and
in the server's timezone otherwise.

The default for TagAttrs is 'a' and it should always be a tag that shows the
content of the title attribute as a tooltip.

See GenerateDateTime() for more details.

=back
=cut

sub GenerateTimeTipDate($;$$)
{
  my ($Sec1970, $Class, $TagAttrs) = @_;

  if (defined $Sec1970)
  {
    my $Tag;
    if ($TagAttrs)
    {
      $Tag = $TagAttrs;
      $Tag =~ s/ .*$//;
    }
    else
    {
      $TagAttrs = $Tag = "a";
    }
    $Class = "$Class " if ($Class);
    print "<$TagAttrs class='${Class}timetipdate' timestamp='$Sec1970' ",
          strftime("title='%Y-%m-%d'>%H:%M:%S", localtime($Sec1970)),
          "</$Tag>";
  }
  else
  {
    print "&nbsp;";
  }
}


#
# Property value formatting
#

=pod
=over 12

=item C<GenerateValueHTML()>

Generates an HTML snippet representing the value in a user-readable form.

The Parent parameter must point to a class that implements the escapeHTML()
method.

The format parameter can be used to specify how to display the specified
value and should have one of the basic property descriptor types with a couple
of extensions for timestamps:

=over

=item "DT"
Show the date and time mostly in ISO 8601 format. This is the default format
for timestamp properties.

=item "timetipdate"
Same as above but put the date part in a tooltip.

=item "daytimetipdate"
Show the day and time and put the full date as a tooltip.

=back

=back
=cut

sub GenerateValueHTML($$$;$);

sub GenerateValueHTML($$$;$)
{
  my ($Parent, $PropertyDescriptor, $Value, $Format) = @_;

  if ($PropertyDescriptor->GetClass() eq "Basic")
  {
    $Format ||= $PropertyDescriptor->GetType();
    if ($Format eq "B")
    {
      print $Value ? "Yes" : "No";
      return;
    }
    if ($Format eq "DT")
    {
      GenerateDateTime($Value);
      return;
    }
    if ($Format eq "timetipdate")
    {
      GenerateTimeTipDate($Value);
      return;
    }
    if ($Format eq "daytimetipdate")
    {
      GenerateDayTimeTipDate($Value);
      return;
    }
  }
  elsif ($PropertyDescriptor->GetClass() eq "Itemref" and defined $Value)
  {
    my $First = 1;
    foreach my $ValuePD (@{$Value->GetPropertyDescriptors()})
    {
      if ($ValuePD->GetIsKey())
      {
        print "/" if (!$First);
        $First = 0;
        my $PropertyName = $ValuePD->GetName();
        GenerateValueHTML($Parent, $ValuePD, $Value->$PropertyName);
      }
    }
    return;
  }
  print defined $Value ? $Parent->escapeHTML($Value) : "&nbsp;";
}

1;
