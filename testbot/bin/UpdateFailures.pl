#!/usr/bin/perl -Tw
# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
#
# This script updates the failures bug information.
#
# Copyright 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

sub BEGIN
{
  if ($0 !~ m=^/=)
  {
    # Turn $0 into an absolute path so it can safely be used in @INC
    require Cwd;
    $0 = Cwd::cwd() . "/$0";
  }
  if ($0 =~ m=^(/.*)/[^/]+/[^/]+$=)
  {
    $::RootDir = $1;
    unshift @INC, "$::RootDir/lib";
  }
}
my $Name0 = $0;
$Name0 =~ s+^.*/++;

my $WineBugUrl = "https://bugs.winehq.org/";

use Text::CSV::Encoded;

use ObjectModel::Collection;
use WineTestBot::Failures;
use WineTestBot::Log;


#
# Logging and error handling helpers
#

my $Debug;
sub Debug(@)
{
  print STDERR @_ if ($Debug);
}

my $LogOnly;
sub Error(@)
{
  print STDERR "$Name0:error: ", @_ if (!$LogOnly);
  LogMsg @_;
}


#
# Bugzilla interface
#

sub CollectBugInfo($)
{
  my ($BugIds) = @_;

  my $Bugs = {};
  my $Cmd = "wget -qO- '$WineBugUrl/buglist.cgi?bug_id=". join(",", @$BugIds)
            ."&columnlist=bug_status,resolution,short_desc&ctype=csv'";
  if (open(my $Fh, "-|", $Cmd))
  {
    <$Fh>;  # skip the header line

    my $Csv = Text::CSV::Encoded->new({ binary => 1 });
    while (my $Line = <$Fh>)
    {
      if ($Csv->parse($Line))
      {
        my ($BugId, $Status, $Resolution, $Description) = $Csv->fields();
        $Bugs->{$BugId} = {
          Status => $Status,
          Resolution => $Resolution,
          Description => $Description,
        };
        Debug("Bug $BugId - $Status - $Resolution - $Description\n");
      }
      else
      {
        Error("could not parse line: ", $Csv->error_input, "\n");
      }
    }
    close($Fh);
  }
  return $Bugs;
}


#
# Main
#

my (@FailureIds, $Usage);
while (@ARGV)
{
  my $Arg = shift @ARGV;
  if ($Arg eq "--log-only")
  {
    $LogOnly = 1;
  }
  elsif ($Arg eq "--debug")
  {
    $Debug = 1;
  }
  elsif ($Arg =~ /^(?:-\?|-h|--help)$/)
  {
    $Usage = 0;
    last;
  }
  elsif ($Arg =~ /^([0-9]+)$/)
  {
    push @FailureIds, $1;
  }
  else
  {
    Error "unexpected argument '$Arg'\n";
    $Usage = 2;
    last;
  }
}
# Check parameters
if (defined $Usage)
{
  print "Usage: $Name0 [--log-only] [--debug] [--help] [FID...]\n";
  exit $Usage;
}

my $Failures = CreateFailures();
@FailureIds = @{$Failures->GetKeys()} if (!@FailureIds);
@FailureIds = sort { $a <=> $b } @FailureIds;

# Deduplicate the bug ids
my $BugIds;
foreach my $FailureId (@FailureIds)
{
  my $Failure = $Failures->GetItem($FailureId);
  $BugIds->{$Failure->BugId} = 1;
}

# Update the failures bug information
my $Bugs = CollectBugInfo([keys %$BugIds]);
foreach my $FailureId (@FailureIds)
{
  my $Failure = $Failures->GetItem($FailureId);
  my $Bug = $Bugs->{$Failure->BugId};
  if ($Bug)
  {
    if ($Failure->BugStatus ne "deleted")
    {
      my $Resolution = $Bug->{Resolution} || " ---";
      $Resolution = $Resolution eq " ---" ? "" : " $Resolution";
      $Failure->BugStatus("$Bug->{Status}$Resolution");
    }
    $Failure->BugDescription($Bug->{Description});
  }
  else
  {
    $Failure->BugStatus("Does not exist");
    $Failure->BugDescription("The WineHQ bug ". $Failure->BugId ." does not exist!");
  }
  if ($Debug and $Failure->GetIsModified())
  {
    Debug("Failure $FailureId - ", $Failure->BugStatus, " - ", $Failure->BugDescription, "\n");
  }
}

my ($ErrKey, $ErrProperty, $ErrMessage) = $Failures->Save();
if ($ErrMessage)
{
  Error("Could not save $ErrKey because of the $ErrProperty field: $ErrMessage\n");
  exit 1;
}

exit 0;
