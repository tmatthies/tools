<?php

$id = (int)$_GET['id'];
$max = max(0, min(15, (int)$_GET['max']));

if ($id < 0)
        $id = 0;

if ($id < $max)
{
        $new_url = sprintf("%s://%s%s?id=%d&max=%d", $_SERVER['REQUEST_SCHEME'], $_SERVER['HTTP_HOST'], $_SERVER['SCRIPT_NAME'], $id + 1, $max);
        Header("Location: " . $new_url);
}

Header("Content-Type: text/plain");
echo($id);
?>
