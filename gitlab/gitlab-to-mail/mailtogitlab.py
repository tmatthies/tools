#!/usr/bin/env -S python3 -B

from email import message_from_binary_file
from urllib.parse import urljoin

import re
import sys
import email.utils
import requests
from util import fetch_all, Settings

settings = Settings(sys.argv[1])
MIN_PYTHON = (3, 8)  # because of the walrus operator use

if sys.version_info < MIN_PYTHON:
    sys.exit("Python %s.%s or later is required.\n" % MIN_PYTHON)


def log(mail, text):
    print(text)
    if settings.DEBUG:
        print(mail)

def err(mail, text):
    print("Error: {}:\n{}\n------------------------".format(text, mail))

def it_is_email_from_the_bridge(mail):
    from_tuple = email.utils.parseaddr(mail['From'])
    from_email = from_tuple[1]
    return from_email == settings.BRIDGE_FROM_EMAIL


def find_mr_in_header(header):
    project = re.sub(r'/', '-', settings.GITLAB_PROJECT_NAME)

    if match := re.match(r'<%s.*-mr(\d+)-.*@gitlab-mail-bridge>' % project, header):
        return match[1]
    return None


def find_mr(mail):
    in_reply_to = mail.get('In-Reply-To', '')
    references = mail.get('References', '')

    if mr := find_mr_in_header(in_reply_to):
        return mr

    for reference in references.split():
        if mr := find_mr_in_header(reference):
            return mr

    return None


def get_email_body(mail):
    charset = mail.get_content_charset(failobj='UTF-8')
    if mail.is_multipart():
        result = ""
        for part in mail.walk():
            if part.get_content_type() == "text/plain":
                payload = part.get_payload(decode=True)
                # Skip things that look like a mailman tail
                if not re.search(r"^--[ ]*[\r\n]*.*mailing list[ \r\n]*.*[\r\n]^https", payload.decode(), re.MULTILINE):
                    result += payload.decode(encoding=charset)
        return result
    else:
        if mail.get_content_type() != "text/plain":
            return ""

        payload = mail.get_payload(decode=True)
        return payload.decode(encoding=charset)

def escape_body(body):
    return body.replace("```", '\`\`\`')

def remove_mail_footer(body):
    return re.sub(r"--[ ]*[\r\n]*.*mailing list[ \r\n]*.*[\r\n]*https.*$", "", body)

def trim_trailing(body):
    lines = body.splitlines()
    for i in range(len(lines)-1, -1, -1):
        if re.search(r"^>", lines[i]):
            continue
        if len(lines[i].strip()) == 0:
            continue
        break
    return "\n".join(lines[:i+1])


def format_note(mail):
    from_tuple = email.utils.parseaddr(mail['From'])
    from_author = from_tuple[0]
    body = get_email_body(mail)
    if len(body) == 0:
        body = "Email did not contain any text/plain parts.  Please check the mailing list archives."

    body = escape_body(body)
    body = remove_mail_footer(body)
    body = trim_trailing(body)

    return f"{from_author} replied on the mailing list:\n```\n{body}\n```"

def find_reply_to(mail, mr_iid):
    if 'In-Reply-To' in mail:
        m = re.search(r'.*mr([0-9]*).*note([0-9]*)', mail['In-Reply-To'])
        if m and len(m.groups()) == 2 and m.group(1) == mr_iid:
            return int(m.group(2))
    return None


def parse(mail):
    if it_is_email_from_the_bridge(mail):
        log(mail, "Send by the bridge, skipping")
        return None
    if not (mr_iid := find_mr(mail)):
        log(mail, "In-Reply-To and References checked - likely not a reply to one of the bridge emails skipping")
        return None
    if not (note := format_note(mail)):  # couldn't find usable content
        log(mail, "Unable to find usable content in the email, skipping message")
        return None
    replyto = find_reply_to(mail, mr_iid)

    return (mr_iid, note, replyto)


def find_discussion(mr_iid, note_id):
    url = urljoin(settings.GITLAB_URL, f"api/v4/projects/{settings.GITLAB_PROJECT_ID}/merge_requests/{mr_iid}/discussions")
    discussions = fetch_all(url, settings)
    for d in discussions:
        if 'notes' in d:
            for n in d['notes']:
                if n['id'] == note_id:
                    return d['id']
    return None


def post_discussion_note(mr_iid, note, replyto):
    url = urljoin(settings.GITLAB_URL, f"api/v4/projects/{settings.GITLAB_PROJECT_ID}/merge_requests/{mr_iid}/discussions")
    if replyto:
        did = find_discussion(mr_iid, replyto)
        if did:
            url += f"/{did}/notes"
    if settings.READ_ONLY:
        print(f"Post: {url} (note_id: {replyto})\nBody:\n{note}\n---\n")
    else:
        r = requests.post(url, headers={"PRIVATE-TOKEN": settings.GITLAB_TOKEN}, data={'body': note, 'note_id': replyto})
        r.raise_for_status()


def main():
    mail = message_from_binary_file(sys.stdin.buffer)
    if not (mr_iid_and_note := parse(mail)):
        return
    try:
        post_discussion_note(*mr_iid_and_note)
        log(mail, "Added note from {} re {}".format(mail['From'], mail['Subject']))
    except:
        err(mail, "Failed to add note from {} re {}".format(mail['From'], mail['Subject']))
        exit(1)


if __name__ == "__main__":
    main()
