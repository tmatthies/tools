#!/usr/bin/env python3

# runner:  run gitlabtomail.py periodically, or on request

from http.server import BaseHTTPRequestHandler, HTTPServer
import subprocess
import sys
import os


NAME = ''
PORT = 8000
CMD = "./gitlabtomail.py"
ARGV1 = sys.argv[1]
TIMEOUT = 600   # 10 minutes

class my_handler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        print("Request: " + self.path)
        self.wfile.write(bytes("<html><body></body></html>", "utf-8"))
        subprocess.call([ CMD, ARGV1 ])

    def do_POST(self):
        self.do_GET()


if __name__ == "__main__":        
    os.chdir(os.path.dirname(sys.argv[0]))
    webServer = HTTPServer((NAME, PORT), my_handler)
    webServer.timeout = TIMEOUT
    webServer.request_queue_size = 4
    webServer.allow_reuse_address = True
    print("Server started http://%s:%s" % (NAME, PORT))

    while True:
        try:
            webServer.handle_request()
        except KeyboardInterrupt:
            break
        subprocess.call([ CMD, ARGV1 ])

    webServer.server_close()
    print("Server stopped.")
